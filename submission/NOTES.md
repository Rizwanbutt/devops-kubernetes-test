## Project Structure

* global resources (like ingress) are created in `resources` directory
* namespaced resources are created in `submission` directory

## Deployment instructions

* Clone the repository
* First create global resources
> kubectl apply -f resources
* For deployment, first create helm package for the application
> helm package submission
* Deploy the application in dev environment
> helm upgrade -i -n vp-app-dev test-app test-app-0.1.0.tgz
* Promote the code to prod
> helm upgrade -i -n vp-app-prod test-app test-app-0.1.0.tgz
* Upgrade the application version (nginx version in this case)
> helm upgrade -n vp-app-dev test-app test-app-0.1.0.tgz --set spec.tag=1.12.2


## Kubernetes objects created

* configmaps
* deployment (stateless)
* hpa (horizontal pod autoscaler)
* services

### ConfigMaps

ConfigMaps are created to pass nginx web server configuration and an HTML web page dynamically to the deployment. It's created in each namespace (vp-app-dev and vp-app-prod) for deployments

### Deployment

Deployment is created to deploy nginx image in kubernetes. It uses configmaps as volumes to mount nginx web server configurations and HTML web page to host.

The deployment is stateless so can be deployed in any kubernetes cluster.

Deployment is using default deployment strategy (i.e RollingUpdate with maxSurge: 25% and maxUnavailable: 25%) and it can be changed to other deployment stratigies (like Recreate)

Also deployment has minimum 3 replicas and fault-tolerant, means if any of the pod is down, it will automatically be created by replication controller so we have minimum of 3 pods all the time

### HPA (horizontal pod autoscaler)

HPA is configured to scale the deployment horizontally. HPA monitors the CPU utilization and scales the pods between 3 and 5 (these numbers can be configured in hpa.yaml)

### Services

As per `resources/base.yaml`, the ingress route `/dev` is pointing to the `vp-app` service and `/prod` route is pointing to the `vp-app` service.

So Services are as a single point of entry for the deployment from ingress. It distributes the traffic between pods equally.
